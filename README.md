# Support

Issue tracker and knowledge-base for Jabber/XMPP user-support questions.

## Issue tracker

Intended to collect common Jabber/XMPP problems that can not easily solved in the support chat... sort of as a "known issues" list one can point to and also for developers to look at to maybe improve.

## General support channel

If you have general support question please join us on [support@joinjabber.org](xmpp:support@joinjabber.org?join). If you don't have a working Jabber/XMPP client yet, you can also use our [simple webclient](https://chat.joinjabber.org/#/guest?join=support); no registration required.

## Server admin support channel

If you are interested in running your own Jabber/XMPP server or are having issues with your server you can also join our dedicated channel here: [servers@joinjabber.org](xmpp:servers@joinjabber.org?join). If you broke your Jabber/XMPP server and have no working account on another server, you can also use our [simple webclient](https://chat.joinjabber.org/#/guest?join=servers).